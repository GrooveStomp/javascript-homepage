//------------------------------------------------------------------------------
// Create a namespace for all my website-specific functionality.
//
var OmanThatsGood = function() {

    //----------------------------------------------------------------------
    // This takes the html out from the given selected html/dom element,
    // and repeats it for every item in array, applying function map to
    // each element in array in the process.
    //
    var repeat_html_with = function($sel, array, map) {
        var html = $sel.html();
        $sel.html("");

        for (var i=0; i < array.length; i++) {
            $sel.append(html);
            map(array[i]);
        }
    };

    return {

        //----------------------------------------------------------------------
        // The main entry point to my website.  This simply performs a json-
        // http-request (JHR) for each of my data providers.
        //
        init: function() {
            var providers = ['github', 'twitter', 'tumblr'];
            for (var i = 0; i < providers.length; i++) {
                OmanThatsGood.get_provider(providers[i]);
            }
        },

        //----------------------------------------------------------------------
        // The "generic" JHR handler for each JHR provider.  This does some
        // basic setup, then delegates to the specific handler for the given
        // provider.
        //
        get_provider: function(provider) {
            $.ajax({
                url: "/" + provider + ".json",
                type: "GET",
                dataType: "json",
                success: function(data) {
                    $obj = $("#" + provider);
                    $obj.css("display", "");
                    OmanThatsGood["handle_" + provider]($obj, data);
                },
                error: function(data) {
                    $obj = $("#" + provider);
                    OmanThatsGood["handle_" + provider]($obj, data, true);
                }
            });
        },

        //----------------------------------------------------------------------
        // GitHub JHR handler.
        //
        handle_github: function($obj, data, failure) {
            if (undefined != failure) {
                $obj.append(" Failure!");
            }
            else {
                var $link = $obj.children("span").children("a");
                $link.append(data.user.name);
                $link.attr("href", data.user.url);

                var $ul = $obj.children("ul");
                var repos = data.repositories;

                repeat_html_with($ul, repos, function(item) {
                    var $el = $ul.children("li").last();
                    $el.children("a").attr("href", item.url);
                    $el.children("a").append(item.name);
                    $el.children("span").append(item.time);
                });
            }
        },

        //----------------------------------------------------------------------
        // Twitter JHR handler.
        //
        handle_twitter: function($obj, data, failure) {
            if (undefined != failure) {
                $obj.append(" Failure!");
            }
            else {
                var $link = $obj.children("span").children("a");
                $link.append(data.user.name);
                $link.attr("href", data.user.url);

                var $ul = $obj.children("#twitter_mentions").children("ul");
                var tweets = data.mentions;

                repeat_html_with($ul, tweets, function(item) {
                    var $el = $ul.children("li").last();
                    $el.children("span").append(item.text);
                    $el.children("a").append(item.user.screen_name);
                    $el.children("a").attr(
                        "href",
                        "http://twitter.com/#!/" + item.user.screen_name
                    );
                });

                $ul = $obj.children("#twitter_tweets").children("ul");
                tweets = data.timeline;

                repeat_html_with($ul, tweets, function(item) {
                    var $el = $ul.children("li").last();
                    $el.children("span").append(item.text);
                });
            }
        },

        //----------------------------------------------------------------------
        // Tumblr JHR handler.
        //
        handle_tumblr: function($obj, data, failure) {
            $obj.append("Tumblr fetched!");
            if (undefined != failure) {
                $obj.append(" Failure!");
            }
        }
    };

}();
