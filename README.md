This is an old webpage I had created for myself, mostly as a way to play with heavier Javascript interactions.

The most interesting bit lies in how I did Javascript templating.
Take a look at
[public/js/main.js](https://bitbucket.org/GrooveStomp/javascript-homepage/src/ffd2a2ac632a1261124c3cefeebdbe52b7fff662/public/js/main.js?at=master)
for how the actual template replacement works, and look at
[views/index.html](https://bitbucket.org/GrooveStomp/javascript-homepage/src/ffd2a2ac632a1261124c3cefeebdbe52b7fff662/views/index.erb?at=master)
for the actual template placeholders.

I got the idea from Clojure's templating systems.  It may have been Enlive or Hiccup, but a quick glance makes me think no.  If I find the actual inspiration, I will update this accordingly.

TODO!
* StackOverflow profile link.
* Kaggle profile link.
* LinkedIn profile link.
* Logos for all the above.
