require 'hubruby'
require 'json'

class GitHubHelper
  @@username = 'GrooveStomp'
  @@url_base = 'https://github.com/'

  def self.info
    user = GitHub.user(@@username)
    reps = []
    repos = user.repositories
    repos.each do |r|
      t = Time.parse(r.pushed_at)
      r.time = "#{t.day}/#{t.month}/#{t.year}"
      r.sort_time = "#{t.year}#{t.month}#{t.day}"
      reps.push({ :url => r.url,
                  :name => r.name,
                  :time => r.time,
                  :sort_time => r.sort_time })
    end
    reps.sort! { |a, b| b[:sort_time] <=> a[:sort_time] }

    JSON.generate({ :user => { :name => @@username,
                               :url => "#{@@url_base}#{@@username}" },
                    :repositories => reps })
  end

end
