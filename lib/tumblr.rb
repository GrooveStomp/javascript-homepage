require 'tumblr'
require 'json'

#-------------------------------------------------------------------------------
#
class TumblrHelper
  @@url = 'omanthatsgood.tumblr.com'

  #-----------------------------------------------------------------------------
  #
  def self.info
    raw_posts = []
    Tumblr::API.read(@@url) do |pager|
      raw_posts = pager.page(0).posts[0,10]
    end

    posts = []
    raw_posts.each do |post|
      if post.type.to_s == 'Tumblr::Data::Photo'
        posts.push({ :title => post.caption,
                     :date => "#{post.date.day}/#{post.date.month}/#{post.date.year}",
                     :body => post.urls[400],
                     :type => :image })
      elsif post.type.to_s == 'Tumblr::Data::Link'
        posts.push({ :title => post.description,
                     :date => "#{post.date.day}/#{post.date.month}/#{post.date.year}",
                     :body => post.url,
                     :type => :link })
      elsif post.type.to_s == 'Tumblr::Data::Regular'
        posts.push({ :title => post.title,
                     :date => "#{post.date.day}/#{post.date.month}/#{post.date.year}",
                     :body => post.body,
                     :type => :post })
      end
    end

    JSON.generate({ :posts => posts,
                    :url => @@url })
  end

end
