require 'rubygems'
require 'bundler'

Bundler.require

require './routes.rb'

run Sinatra::Application
