#-------------------------------------------------------------------------------
# High-level includes
#-------------------------------------------------------------------------------
# Basic Sinatra Requires
require 'rubygems'
require 'bundler/setup'
require 'sinatra'
# Views
require 'erb'
require 'less'
# Internal Libraries
require 'lib/github'
require 'lib/twitter'
require 'lib/tumblr'

#-------------------------------------------------------------------------------
# Routes.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#
get '/' do
  erb :index, :locals => { :data => 'blah' }
end
#-------------------------------------------------------------------------------
#
get '/github.json' do
  erb :json, :locals => { :data => GitHubHelper.info}
end
#-------------------------------------------------------------------------------
#
get '/twitter.json' do
  erb :json, :locals => { :data => TwitterHelper.info }
end
#-------------------------------------------------------------------------------
#
get '/tumblr.json' do
  erb :json, :locals => { :data => TumblrHelper.info }
end
#-------------------------------------------------------------------------------
#
get %r{^/css/([\w]+).css} do |style|
  less "css/#{style}".to_sym
end
